<?php

namespace App\Repository;

use App\Entity\Problematic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Problematic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Problematic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Problematic[]    findAll()
 * @method Problematic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProblematicRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Problematic::class);
    }

    public function countElements()
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select($qb->expr()->count('p'));
        return $qb->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function findProblemBetween($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere(":value > p.publishedAt")
            ->andWhere(":value < (CASE p.durationUnit WHEN 'month' THEN DATE_ADD(p.publishedAt, p.durationValue , 'month') WHEN 'week' THEN DATE_ADD(p.publishedAt, p.durationValue, 'week') WHEN 'day' THEN DATE_ADD(p.publishedAt, p.durationValue, 'day') ELSE p.publishedAt END)")
            ->setParameter('value', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllNextProbl(\DateTime $startDate): array
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.publishedAt > :startDate')
            ->orderBy('p.publishedAt', 'ASC')
            ->setParameter('startDate', $startDate)
            ->getQuery()
            ->getResult()
        ;
    }

}
