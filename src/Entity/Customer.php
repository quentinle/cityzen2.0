<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 */
class Customer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var \DateTime $create_at
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pseudo;

    /**
     * 
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="customer", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * 
     * @ORM\OneToMany(targetEntity=Problematic::class, mappedBy="customer")
     */
    private $problematics;

    /**
     * @ORM\OneToMany(targetEntity=Solution::class, mappedBy="customer")
     */
    private $solutions;

    /**
     * @ORM\OneToMany(targetEntity=Voted::class, mappedBy="customer")
     */
    private $voteds;

    public function getDate()
    {
        $this->createdAt = new \DateTime();
    }

    public function __construct()
    {
        $this->problematics = new ArrayCollection();
        $this->solutions = new ArrayCollection();
        $this->voteds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }



    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(?string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Problematic[]
     */
    public function getProblematics(): Collection
    {
        return $this->problematics;
    }

    public function addProblematic(Problematic $problematic): self
    {
        if (!$this->problematics->contains($problematic)) {
            $this->problematics[] = $problematic;
            $problematic->setCustomer($this);
        }

        return $this;
    }

    public function removeProblematic(Problematic $problematic): self
    {
        if ($this->problematics->contains($problematic)) {
            $this->problematics->removeElement($problematic);
            // set the owning side to null (unless already changed)
            if ($problematic->getCustomer() === $this) {
                $problematic->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Solution[]
     */
    public function getSolutions(): Collection
    {
        return $this->solutions;
    }

    public function addSolution(Solution $solution): self
    {
        if (!$this->solutions->contains($solution)) {
            $this->solutions[] = $solution;
            $solution->setCustomer($this);
        }

        return $this;
    }

    public function removeSolution(Solution $solution): self
    {
        if ($this->solutions->contains($solution)) {
            $this->solutions->removeElement($solution);
            // set the owning side to null (unless already changed)
            if ($solution->getCustomer() === $this) {
                $solution->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Voted[]
     */
    public function getVoteds(): Collection
    {
        return $this->voteds;
    }

    public function addVoted(Voted $voted): self
    {
        if (!$this->voteds->contains($voted)) {
            $this->voteds[] = $voted;
            $voted->setCustomer($this);
        }

        return $this;
    }

    public function removeVoted(Voted $voted): self
    {
        if ($this->voteds->contains($voted)) {
            $this->voteds->removeElement($voted);
            // set the owning side to null (unless already changed)
            if ($voted->getCustomer() === $this) {
                $voted->setCustomer(null);
            }
        }
        return $this;
    }

    public function __tostring()
    {
       return $this->getFirstName();
    }

    
}
