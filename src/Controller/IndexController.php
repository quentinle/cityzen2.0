<?php

namespace App\Controller;

use App\Entity\Upload;
use App\Form\UploadType;
use App\Repository\ProblematicRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends AbstractController
{
   /**
     * @Route("/citizen_publication", name = "citizen_publication")
     */
    public function index(Request $request)
    {   
        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $file = $upload->getName();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $upload->setName($fileName);

            return $this->redirectToRoute('home');
        }
        return $this->render('index/index.html.twig', array(
            'form' => $form->createView(),
        ));  

    }

    /**
     * @Route("/", name = "home")
     */
    public function home(ProblematicRepository $poblematicRepo) 
    {
        $actualProblematic = $poblematicRepo->findProblemBetween(new \DateTime());
        //recuperer la problematique en cour par rapport a la date actuelle
        dump($actualProblematic);
        //recuperer les problematique en cours de vote
        return $this->render('index/home.html.twig',[
            'actualProblematic' => $actualProblematic,
        ] );
    }
}
