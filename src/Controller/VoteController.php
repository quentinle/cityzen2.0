<?php

namespace App\Controller;


use App\Repository\SolutionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VoteController extends AbstractController
{
    /**
     * @Route("/vote", name="vote")
     * @param SolutionRepository $solutionRepository
     * @return Response
     */
    public function index(SolutionRepository $solutionRepository)
    {
        return $this->render('vote/index.html.twig', ["datas"=>$solutionRepository->findAll()]);
    }


}
