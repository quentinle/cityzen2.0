# Welcome to our website *Citizen2.0*

---------------------------------------
when you clone the repository

## Pré requis
- Symfony 5.0
- Npm 6.14
- php 7.4

## Configuration
 - Configure the .env (or .env.local) File
   Open the .env file and make any adjustments you need - specifically
   **DATABASE_URL** and **MAILER_DSN** Or, if you want, you can create a .env.local file
   and override any configuration you need there (instead of changing
   .env directly).

## Tape this command
- $ composer install
- $ php bin/console doctrine:database:create
- $ php bin/console doctrine:migrations:migrate
- $ npm install 
- $ npm run dev
- $ php bin/console server:start or symfony:serve
